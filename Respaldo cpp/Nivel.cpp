#include "Nivel.h"

#include <Bengine\Errors.h>
#include <Bengine\ResourceManager.h>

#include <fstream>
#include <iostream>

const int ANCHO_BALDOSAS = 20;
const int CABECERA_NIVEL = 5;

Nivel::Nivel(const std::string& direccionArchivo)
{
	std::ifstream archivo;
	archivo.open(direccionArchivo);

	//Validacion de errores de carga
	if (archivo.fail()){
		Bengine::fatalError("Error al cargar el nivel en " + direccionArchivo);
	}

	//Eliminamos la cabecera para leer solo lo que nos interesa.
	std::string tmp;
	for (int i = 0; i < CABECERA_NIVEL; i++){
		std::getline(archivo, tmp);
	}

	//Ahora se lee el formato del nivel linea a linea y se almacena en un vector de strings
	while (std::getline(archivo, tmp)){
		_nivelData.push_back(tmp);
	}

	_spriteBatch.init();
	_spriteBatch.begin();

	//Rectangulo con las coordenadas relativas de las texturas con respecto a los sprites
	glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	//Renderizar todas las baldosas
	for (int y = 0; y < _nivelData.size(); y++){
		for (int x = 0; x < _nivelData[y].size(); x++){

			//Se toma la baldosa
			char baldosa = _nivelData[y][x];

			//Se establece el rectangulo con las coordenadas de los sprites respectivos
			glm::vec4 destRect(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS);

			Bengine::Color color;
			color.r = 0;
			color.g = 0;
			color.b = 225;
			color.a = 255;

			//Por el momento solo consideramos a los muros para dibujarlos.
			switch (baldosa){
			case 'M':
				_spriteBatch.draw(destRect, 
								uvRect,
								Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
								1.0f,
								color);
				break;
			case '@':
				_posicionPacman.x = x;
				_posicionPacman.y = y;

				break;
			default:
				std::printf("Unexpected symbol %c at (%d,%d)\n", baldosa, x, y);
				break;
			}
		}
	}

	_spriteBatch.end();
}


Nivel::~Nivel()
{
}

 void Nivel::dibujar(){
	 _spriteBatch.renderBatch();
}

 glm::vec2 Nivel::getPosicionPacman(){
	 return _posicionPacman;
 }