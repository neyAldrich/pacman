#include "Agente.h"
#include <Bengine/ResourceManager.h>

Agente::Agente()
{
}


Agente::~Agente()
{
}

void Agente::dibujar(Bengine::SpriteBatch& _spriteBatch){

	static int textureID = Bengine::ResourceManager::getTexture("Texturas/pacmanAlpha.png").id;

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	glm::vec4 destRect;
	destRect.x = _posicion.x;
	destRect.y = _posicion.y;
	destRect.z = ANCHO_AGENTE;
	destRect.w = ANCHO_AGENTE;

	_spriteBatch.draw(destRect, uvRect, textureID, 0.0f, _color);
}