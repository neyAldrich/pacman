#include <Windows.h>

#include "JuegoPrincipal.h"

#include <Bengine\Bengine.h>
#include <Bengine\Timing.h>

#include <SDL/SDL.h>
#include <iostream>

JuegoPrincipal::JuegoPrincipal()
{
	_screenAncho = 650;	
	_screenAlto = 650;	
	_fps = 0;	
	_nivelActual = 0;
	_puntuacion = 0;
	_pacman = nullptr;
	_estadoJuego = EstadoJuego::JUGAR;
}


JuegoPrincipal::~JuegoPrincipal()
{
	//Evitar fugas de memoria
	for (int i = 0; i < _niveles.size(); i++){
		delete _niveles[i];
	}
}


void JuegoPrincipal::arrancar(){
	iniciarSistemas();
	iniciarNiveles();

	lazoJuego();
}


// Inicializar sistemas principales
void JuegoPrincipal::iniciarSistemas(){
	Bengine::init();

	//Creando la ventada del juego
	_window.create("Pacman", _screenAncho, _screenAlto, 0);

	//Estableciendo el color de limpieza de pixeles (Color de fondo de la ventana)
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	iniciarShaders();
	
	_spriteBatchAgentes.init();

	_camara.init(_screenAlto, _screenAlto);
}

// Inicializar los shaders
void JuegoPrincipal::iniciarShaders(){
	//Compila los shaders indicados.
	_programaTextura.compileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");
	_programaTextura.addAttribute("vertexPosition");
	_programaTextura.addAttribute("vertexColor");
	_programaTextura.addAttribute("vertexUV");
	_programaTextura.linkShaders();
}

// Inicializar niveles (Solo uno por el momento)
void JuegoPrincipal::iniciarNiveles(){
	_niveles.push_back(new Nivel("Niveles/nivel1.txt"));
	_nivelActual = 0;

	_pacman = new Pacman();
	_pacman->iniciar(1.0f, _niveles[_nivelActual]->getPosicionPacman());
}



// Lazo del juego
void JuegoPrincipal::lazoJuego(){

	Bengine::FpsLimiter limitadorFPS;
	limitadorFPS.setMaxFPS(30.0f);

	//Lazo para actualizar el estado del juego fotograma a fotograma.
	while (_estadoJuego == EstadoJuego::JUGAR){
		limitadorFPS.begin();

		procesarInput();
		_camara.update();

		dibujarJuego();

		_fps = limitadorFPS.end();
	}

}

// Manaje el procesamiento de entradas.
void JuegoPrincipal::procesarInput(){

	SDL_Event e;

	while (SDL_PollEvent(&e)){
		switch (e.type){

		case SDL_QUIT:
			//Salir del juego
			break;
		case SDL_MOUSEMOTION:
			//Transforma las coordenadas normalizadas a coordenadas del espacio en OpenGl
			_inputManager.setMouseCoords(e.motion.x, e.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(e.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(e.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(e.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(e.button.button);
			break;
		}
	}
}

// Renderiza el juego
void JuegoPrincipal::dibujarJuego(){

	//Establece la profundidad base
	glClearDepth(1.0f);

	//Limpia los b�feres de color y de fondo. 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT /*Operador binario para combinar los bits indicadores*/);

	//Establece que se inicia el uso del administrador de texturas
	_programaTextura.use();

		//Indica que canal de textura se va a utilizar
		glActiveTexture(GL_TEXTURE0);

		//Asignando variables uniformes correspondientes a los shaders.
		GLint textureUniform = _programaTextura.getUniformLocation("mySampler");
		glUniform1i(textureUniform, 0);

		//Obteniendo la matriz para simular la camara.
		glm::mat4 matrizProyeccion = _camara.getCameraMatrix();
		GLint pUniform = _programaTextura.getUniformLocation("P");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &matrizProyeccion[0][0]);

		//Dibujar el nivel
		_niveles[_nivelActual]->dibujar();


		//Dibujar los agentes
		_spriteBatchAgentes.begin();

		_pacman->dibujar(_spriteBatchAgentes);

		_spriteBatchAgentes.end();

		_spriteBatchAgentes.renderBatch();

	//Se finaliza el uso del administrador de texturas
	_programaTextura.unuse();

	_window.swapBuffer();
}
