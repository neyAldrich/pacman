#pragma once

#include <glm\glm.hpp>
#include <Bengine\SpriteBatch.h>
#include <Bengine\Vertex.h>

#include "Enums.h"


const float ANCHO_ELEM_ESTATICO = 20;
const float RADIO_ELEM_ESTATICO = ANCHO_ELEM_ESTATICO / 2;

class ElementoEstatico
{
public:
	ElementoEstatico(glm::ivec2 pos);
	~ElementoEstatico();

	void dibujar(Bengine::SpriteBatch& spriteBatch);

	Bengine::ColorRGBA8 getColor(){ return _color; }
	int getTextureID(){ return _textureID; }

	const glm::vec2 getPosicion() const { return _posicion; }

protected:
	glm::vec2 _posicion;

	int _textureID;

	Bengine::ColorRGBA8 _color;
};
