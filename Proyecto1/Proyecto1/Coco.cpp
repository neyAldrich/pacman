#include "Coco.h"
#include <Bengine\ResourceManager.h>

Coco::Coco(glm::ivec2 pos, TIPO_COCO tipo) : ElementoEstatico(pos)
{
	_tipo = tipo;

	switch (_tipo){
	case TIPO_COCO::NORMAL:
		_textureID = Bengine::ResourceManager::getTexture("Texturas/coco.png").id;
		break;
	case TIPO_COCO::SUPER:
		_textureID = Bengine::ResourceManager::getTexture("Texturas/superCoco.png").id;
		break;
	}
}


Coco::~Coco()
{
	
}
