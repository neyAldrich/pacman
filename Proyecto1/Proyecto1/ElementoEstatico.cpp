#include "ElementoEstatico.h"

#include <Bengine\ResourceManager.h>

ElementoEstatico::ElementoEstatico(glm::ivec2 pos)
{
	_color.setColor(255, 255, 255, 255);

	_posicion = pos;
}


ElementoEstatico::~ElementoEstatico()
{
}

void ElementoEstatico::dibujar(Bengine::SpriteBatch& spriteBatch){

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	glm::vec4 destRect;
	destRect.x = _posicion.x;
	destRect.y = _posicion.y;
	destRect.z = ANCHO_ELEM_ESTATICO;
	destRect.w = ANCHO_ELEM_ESTATICO;

	spriteBatch.draw(destRect, uvRect, _textureID, 0.0f, _color);

}