#pragma once

#include "Agente.h"
#include "Pacman.h"
#include <Bengine\InputManager.h>
#include <vector>
#include <map>

class Fantasma : public Agente
{
	
public:
	Fantasma();
	~Fantasma();


	void iniciar(glm::vec2 posicion, TIPO_FANTASMA tipoFantasma);
	void actualizar(Nivel* nivel , Pacman *pacman);
	void dibujar(Bengine::SpriteBatch& spriteBatch);

	void buscarPacman(Pacman *pacman/*, std::map<std::string, glm::vec2> posicionesMuros*/);

	//void huntGhost(Pacman *pacman, std::map<std::string, glm::vec2> posicionesMuros);


	TIPO_FANTASMA getTipoFantasma() { return _tipoFantasma; }

	void setTexturaInicial();
	void setTexturaVulnerable();

	bool estaPacmanCerca(Pacman* pacman, float delta);

	bool boolColisionarPuerta;
	bool boolEnPrision;

private:
	int _frames;

	TIPO_FANTASMA _tipoFantasma;

	float _deltaCercania;

	bool boolMovAleatorio;


	std::vector<glm::vec2> obtenerDireccionesHaciaElemento(glm::vec2 posElemento);

	Pacman* obtenerPacmanCercano(Pacman& pacman);

	glm::vec2 _oldPosicion;
	glm::vec2 _oldDireccion;
	glm::vec2 _nextDireccion;
};