#pragma once

#include "Agente.h"
#include "Coco.h"
#include <Bengine\InputManager.h>

#include "ElementoEstatico.h"

class Nivel;

class Pacman : public Agente 
{
public:
	Pacman(int vida = 3);
	~Pacman();

	int _vidas;

	void iniciar(float velocidad, glm::vec2 posicion, Bengine::InputManager* inputManager);
	//void iniciar(float velocidad, glm::vec2 posicion, Bengine::InputManager* inputManager, glm::vec2 direccion);

	void actualizar(Nivel* nivel);
	void dibujar(Bengine::SpriteBatch& spriteBatch);

	bool colisionarConElemento(ElementoEstatico* coco);

	void modificarTiempoPoder(int delta);
	int getTiempoPoder() { return _tiempoRestantePoder; }

	//
	void animacion();
	//

	void activarPoder(int duracion /*en fotogramas*/);
	bool getPoderActivo() { return _poderActivo; }

private:
	
	int _frames;

	Bengine::InputManager* _inputManager;

	bool _poderActivo;
	int _tiempoRestantePoder;

	glm::vec2 _oldPosicion;
	glm::vec2 _oldDireccion;
	glm::vec2 _nextDireccion;

	
};


