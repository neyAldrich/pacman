#include <Windows.h>

#include "JuegoPrincipal.h"
#include "Enums.h"

#include <Bengine\Bengine.h>
#include <Bengine\Timing.h>
#include <Bengine\BengineErrors.h>

#include <SDL/SDL.h>
#include <iostream>
#include <stdlib.h>

JuegoPrincipal::JuegoPrincipal()
{
	_screenAncho = 950;	
	_screenAlto = 650;	
	_fps = 0;	
	_nivelActual = 0;
	_puntuacion = 0;
	_cocosComidos = 0;
	_pacman = nullptr;
	_fruta = nullptr;
	_boolMostrarFruta = false;

	_estadoJuego = EstadoJuego::JUGAR;

	_acumuladorFotogramas = 0;
	_acumuladorTiempo = 0;
}


JuegoPrincipal::~JuegoPrincipal()
{
	//Evitar fugas de memoria
	for (int i = 0; i < _niveles.size(); i++){
		delete _niveles[i];
	}
}

//Arranca el juego.
void JuegoPrincipal::arrancar(){
	iniciarSistemas();
	iniciarNiveles();

	Bengine::Music introMusic = _audioEngine.loadMusic("Sound/pacman_beginning.wav");
	introMusic.play();

	lazoJuego();
}

/**
*	Inicializa los elementos principales de la aplicaci�n
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::iniciarSistemas(){
	//Iniciar el motor de juego
	Bengine::init();

	//Iniciar sonido;
	_audioEngine.init();	

	//Creando la ventada del juego
	_window.create("Pacman", _screenAncho, _screenAlto, 0);

	//Estableciendo el color de limpieza de pixeles (Color de fondo de la ventana)
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); ///< Negro

	iniciarShaders();
	
	_spriteBatchAgentes.init();
	_spriteBatchCocos.init();
	_spriteBatchHud.init();

	//Iniciar SpriteFont
	_spriteFont = new Bengine::SpriteFont("Fonts/crackman/crackman.ttf", 64);

	//Iniciar camara
	_camara.init(_screenAncho, _screenAlto);
}

/**
*	Inicia los shaders.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::iniciarShaders(){
	//Compila los shaders indicados.
	_programaTextura.compileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");
	_programaTextura.addAttribute("vertexPosition");
	_programaTextura.addAttribute("vertexColor");
	_programaTextura.addAttribute("vertexUV");
	_programaTextura.linkShaders();
}


/**
*	Inicia los niveles y todos los elementos pertenecientes a estos.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::iniciarNiveles(){
	const float GHOST_SPEED = 4.0F;
	const float VELOCIDAD_PACMAN = 3.0f;

	//Iniciar efectos de sonido
	_soundCocoComido = _audioEngine.loadSoundEffect("Sound/pacman_chomp.wav");
	_soundFrutaComida = _audioEngine.loadSoundEffect("Sound/pacman_eatfruit.wav");
	_soundFantasmaComido = _audioEngine.loadSoundEffect("Sound/pacman_eatghost.wav");
	_soundPacmanMuerto = _audioEngine.loadSoundEffect("Sound/pacman_death.wav");
	_soundVidaExtra = _audioEngine.loadSoundEffect("Sound/pacman_extrapac.wav");

	_niveles.push_back(new Nivel("Niveles/nivel3.txt"));
	_nivelActual = 0;

	_pacman = new Pacman();
	_pacman->iniciar(VELOCIDAD_PACMAN, _niveles[_nivelActual]->getPosicionInicialPacman(), &_inputManager);

	//Iniciar Fantasmas
	_fantasmas.emplace_back(new Fantasma());
	_fantasmas.back()->iniciar(_niveles[_nivelActual]->getPosInicialFantasmas().at(TIPO_FANTASMA::ROSA), TIPO_FANTASMA::ROSA);

	_fantasmas.emplace_back(new Fantasma());
	_fantasmas.back()->iniciar(_niveles[_nivelActual]->getPosInicialFantasmas().at(TIPO_FANTASMA::ROJO), TIPO_FANTASMA::ROJO);

	_fantasmas.emplace_back(new Fantasma());
	_fantasmas.back()->iniciar(_niveles[_nivelActual]->getPosInicialFantasmas().at(TIPO_FANTASMA::AMARILLO), TIPO_FANTASMA::AMARILLO);

	_fantasmas.emplace_back(new Fantasma());
	_fantasmas.back()->iniciar(_niveles[_nivelActual]->getPosInicialFantasmas().at(TIPO_FANTASMA::CELESTE), TIPO_FANTASMA::CELESTE);


	//Iniciar Cocos
	//normales
	for (int i = 0; i < _niveles[_nivelActual]->getPosicionesCocos().size(); i++){
		Coco* newCoco = new Coco(_niveles[_nivelActual]->getPosicionesCocos()[i], TIPO_COCO::NORMAL);
		_vecCocos.emplace_back(newCoco);
	}

	//super
	for (int i = 0; i < _niveles[_nivelActual]->getPosicionesSuperCocos().size(); i++){
		Coco* newCoco = new Coco(_niveles[_nivelActual]->getPosicionesSuperCocos()[i], TIPO_COCO::SUPER);
		_vecCocos.emplace_back(newCoco);
	}



}


/**
*	Lazo del juego, cada ejecuci�n representa un fotograma
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::lazoJuego(){

	Bengine::FpsLimiter limitadorFPS;
	limitadorFPS.setMaxFPS(FPS);

	//Lazo para actualizar el estado del juego fotograma a fotograma.
	while (_estadoJuego == EstadoJuego::JUGAR){
		limitadorFPS.begin();

		procesarInput();

		actualizarAgentes();


		_camara.setPosition(glm::vec2(280,310));  //<- Ubicar la camara en el centro de la pantalla
		_camara.update();

		if (_cocosComidos == (_niveles[_nivelActual]->getNumCocos() / 4)){
			//Iniciar frutas
			_fruta = new Fruta(_niveles[_nivelActual]->getPosFruta(), TIPO_FRUTA::CEREZA);


		}

		if (_cocosComidos == (_niveles[_nivelActual]->getNumCocos())){
			//Ganar juego
			_estadoJuego = EstadoJuego::GANADO;

		}

		dibujarJuego();

		_acumuladorFotogramas++;

		//Contabilizar el tiempo
		if (_acumuladorFotogramas == 30){
			_acumuladorTiempo++;
			_acumuladorFotogramas = 0;
		}

		_fps = limitadorFPS.end();
	}

	for (int i = 0; i < 500; i++){
		dibujarJuego();
	}

}

/**
*	Maneja la actualizaci�n de posiciones y estados de los agentes en el juego.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo, Oscar Moreno
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::actualizarAgentes(){
	//Nota: hacer la actualizacion una vez que se agregue el vector de agentes.

	_pacman->actualizar(_niveles[_nivelActual]);

	if (_acumuladorFotogramas % 5 == 0){
		if (_pacman != nullptr){
			_pacman->animacion();
		}
	}


	//MEJORAR
	if (_acumuladorTiempo == 2){
		//std::cout << "Puertas Abiertas:" << std::endl;
		for (int i = 0; i < _fantasmas.size(); i++){
			_fantasmas[i]->boolColisionarPuerta = false;
		}

	}


	//Colision entre fantasmas y el escenario
	for (int i = 0; i < _fantasmas.size(); i++){
		_fantasmas[i]->actualizar(_niveles[_nivelActual], _pacman);
		_fantasmas[i]->buscarPacman(_pacman);

	}

	//colision entre fantasmas y pacman
	for (int i = 0; i < _fantasmas.size(); i++){

		if (_pacman->colisionarConAgente(_fantasmas[i]) && _pacman->_vidas > 0)
		{
			if (_pacman->getPoderActivo() == false){

				_pacman->setPosicion(_niveles[_nivelActual]->getPosicionInicialPacman());

				_pacman->dibujar(_spriteBatchAgentes);

				_pacman->_vidas = _pacman->_vidas - 1;

				//std::cout << "Vidas de pacman: " << _pacman->_vidas << std::endl;

				_soundPacmanMuerto.play();
				

				for (int j = 0; j < _fantasmas.size(); j++){
					_fantasmas[j]->setPosicion(_niveles[_nivelActual]->getPosInicialFantasmas().at(_fantasmas[j]->getTipoFantasma()));
					_fantasmas[i]->boolEnPrision = true;
				}
			}
			else{
				glm::vec2 tmpPos = _niveles[_nivelActual]->getPosInicialFantasmas().at(_fantasmas[i]->getTipoFantasma());
				_fantasmas[i]->setPosicion(tmpPos);
				_fantasmas[i]->boolEnPrision = true;

				_soundFantasmaComido.play();
			}
		}
		else if (_pacman->_vidas == 0)
		{
			Bengine::fatalError("You lose");
		}
	}

	
	//colision entre pacman y cocos
	for (int i = 0; i < _vecCocos.size(); i++){
		if (_pacman->colisionarConElemento(_vecCocos[i])){

			//Sonido
			_soundCocoComido.play();

			//std::cout << "Colisionado con coco." << std::endl;
			if (_vecCocos[i]->getTipo() == TIPO_COCO::SUPER){
				if (_pacman->getPoderActivo()){
					_pacman->modificarTiempoPoder(DURACION_PODER * FPS);
				}
				else{
					_pacman->activarPoder(DURACION_PODER * FPS);
				}

				for (int k = 0; k < _fantasmas.size(); k++){
					_fantasmas[k]->setTexturaVulnerable();
				}
			}

			delete _vecCocos[i];
			_vecCocos[i] = _vecCocos.back();
			_vecCocos.pop_back();

			_puntuacion += PUNTUACION_COCO;

			_cocosComidos++;

		}

		//if (_puntuacion > 0 && (_puntuacion % 200) == 0){
		//	_pacman->_vidas += 1;
		//	_soundVidaExtra.play();
		//}
	}
	
	//Colision entre pacman y fruta
	if (_fruta != nullptr){
		if (_pacman->colisionarConElemento(_fruta)){
			_puntuacion += _fruta->getPuntuacion();
			//Sonido
			_soundFrutaComida.play();

			//DEBUG
			//std::cout << "Puntuacion: " << _puntuacion << std::endl;

			delete _fruta;
			_fruta = nullptr;
		}
	}

	if (_pacman->getPoderActivo()){
		///Reducir el contador de tiempo del poder de pacman
		_pacman->modificarTiempoPoder(-1);

		//DEBUG
		//if (_acumuladorFotogramas % 30 == 0){
		//	std::cout << "Tiempo restante poder: " << _pacman->getTiempoPoder() / FPS << " segundos" << std::endl;
		//}
	}
	else{
		for (int k = 0; k < _fantasmas.size(); k++){
			_fantasmas[k]->setTexturaInicial();
		}
	}


}


/**
*	Maneja el procesamiento de entradas por teclado y mouse.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::procesarInput(){

	SDL_Event e;

	while (SDL_PollEvent(&e)){
		switch (e.type){

		case SDL_QUIT:
			_estadoJuego = EstadoJuego::SALIR;
			break;
		case SDL_MOUSEMOTION:
			//Transforma las coordenadas normalizadas a coordenadas del espacio en OpenGl
			_inputManager.setMouseCoords(e.motion.x, e.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(e.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(e.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(e.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(e.button.button);
			break;
		}
	}
}

/**
*	Renderiza el juego.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
void JuegoPrincipal::dibujarJuego(){

	//Establece la profundidad base
	glClearDepth(1.0f);

	//Limpia los b�feres de color y de fondo. 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT /*Operador binario para combinar los bits indicadores*/);

	//Establece que se inicia el uso del administrador de texturas
	_programaTextura.use();

		//Indica que canal de textura se va a utilizar
		glActiveTexture(GL_TEXTURE0);

		//Asignando variables uniformes correspondientes a los shaders.
		GLint textureUniform = _programaTextura.getUniformLocation("mySampler");
		glUniform1i(textureUniform, 0);

		//Obteniendo la matriz para simular la camara.
		glm::mat4 matrizProyeccion = _camara.getCameraMatrix();
		GLint pUniform = _programaTextura.getUniformLocation("P");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &matrizProyeccion[0][0]);

		//Dibujar el nivel
		_niveles[_nivelActual]->dibujar();


		//Dibujar los cocos
		_spriteBatchCocos.begin();

		for (int i = 0; i < _vecCocos.size(); i++){
			_vecCocos[i]->dibujar(_spriteBatchCocos);
		}

		if (_fruta != nullptr){
			_fruta->dibujar(_spriteBatchCocos);
		}

		_spriteBatchCocos.end();

		//Dibujar los agentes
		_spriteBatchAgentes.begin();

		_pacman->dibujar(_spriteBatchAgentes);

		for (int i = 0; i < _fantasmas.size(); i++){
			_fantasmas[i]->dibujar(_spriteBatchAgentes);
		}

		_spriteBatchAgentes.end();


		//Renderizar SpriteBatches
		_spriteBatchCocos.renderBatch();

		_spriteBatchAgentes.renderBatch();


		dibujarHud();

	//Se finaliza el uso del administrador de texturas
	_programaTextura.unuse();

	_window.swapBuffer();
}

/**
*	Renderiza el HUD
*	Creada en 07/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
void JuegoPrincipal::dibujarHud(){
	char buffer[256];
	Bengine::ColorRGBA8 color;
	color.setColor(255, 255, 255, 255);

	_spriteBatchHud.begin();

	sprintf_s(buffer, "%d\nPuntuacion:", _puntuacion);
	_spriteFont->draw(_spriteBatchHud, buffer, glm::vec2(-180, 575), glm::vec2(0.30f), 0.0f, color);

	sprintf_s(buffer, "%d\nVidas:", _pacman->_vidas);
	_spriteFont->draw(_spriteBatchHud, buffer, glm::vec2(-180, 515), glm::vec2(0.30f), 0.0f, color);

	sprintf_s(buffer, "%s\nTiempo Jugado:", formatearTiempo(_acumuladorTiempo).c_str());
	_spriteFont->draw(_spriteBatchHud, buffer, glm::vec2(570, 575), glm::vec2(0.30f), 0.0f, color);

	if (_estadoJuego == EstadoJuego::GANADO){
		sprintf_s(buffer, "HA GANADO!");
		_spriteFont->draw(_spriteBatchHud, buffer, glm::vec2(0, 300), glm::vec2(1.0f), 0.0f, color);
	}


	_spriteBatchHud.end(); 

	_spriteBatchHud.renderBatch();
}


/**
*	Recibe tiempo en segundos y lo formatea a Minutos:Segundos retornandolo en un std::string
*	Creada en 07/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
std::string JuegoPrincipal::formatearTiempo(int tiempo){
	std::string retorno = "";

	int minutos = 0;
	int segundos = 0;

	if (tiempo >= 0){
		minutos = tiempo / 60; //Division entera
		segundos = tiempo % 60;
		
		retorno += std::to_string(minutos) + ":" + std::to_string(segundos);
	}

	return retorno;
}
