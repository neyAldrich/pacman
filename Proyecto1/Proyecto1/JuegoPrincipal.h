/*VERSI�N ACTUAL: 0.5.0*/

#pragma once

#include <Bengine\Window.h>
#include <Bengine\Camera2D.h>
#include <Bengine\GLSLProgram.h>
#include <Bengine\InputManager.h>
#include <Bengine\SpriteBatch.h>
#include <Bengine\AudioEngine.h>
#include <Bengine\SpriteFont.h>


#include <vector>
#include "Nivel.h"
#include "Agente.h"
#include "Pacman.h"
#include "Fantasma.h"
#include "Enums.h"
#include "Coco.h"
#include "Fruta.h"
#include "ElementoEstatico.h"

const int FPS = 30;

const int PUNTUACION_COCO = 10;
const int DURACION_PODER = 10; //Segundos

enum class EstadoJuego{
	JUGAR,
	SALIR,
	GANADO,
};

class JuegoPrincipal
{
public:
	JuegoPrincipal();
	~JuegoPrincipal();

	void arrancar();

private:

	// Inicializar sistemas principales
	void iniciarSistemas();

	// Inicializar niveles (Solo uno por el momento)
	void iniciarNiveles();

	// Inicializar los shaders
	void iniciarShaders();

	// Lazo del juego
	void lazoJuego();

	//Actualizar los agentes
	void actualizarAgentes();

	// Administra el procesamiento de entradas.
	void procesarInput();

	// Renderiza el juego
	void dibujarJuego();

	// Renderiza el HUD
	void dibujarHud();

	// Miembros de las clase

	Bengine::Window _window; ///< Ventana del juego

	Bengine::GLSLProgram _programaTextura; ///< Administrador de carga de texturas

	Bengine::InputManager _inputManager; ///< Maneja la entrada de datos por mouse y teclado

	Bengine::Camera2D _camara; ///< Camara principal

	std::vector<Nivel*> _niveles;  ///< Vector con todos los niveles (1 por el momento)

	std::vector<Fantasma*> _fantasmas;

	std::vector<Coco*> _vecCocos;

	int _cocosComidos;

	Pacman* _pacman;

	Fruta* _fruta;			///< El sprite se almacenara dentro de _spriteBatchCocos
	bool _boolMostrarFruta;

	int _screenAncho;		///< Indica el ancho de la ventana asociada al juego 
	int _screenAlto;		///< Indica el alto de la ventana asociada al juego

	float _fps;				///< Fotogramas por segundo
		
	int _acumuladorFotogramas;
	int _acumuladorTiempo;

	Bengine::SpriteBatch _spriteBatchAgentes;
	Bengine::SpriteBatch _spriteBatchCocos;
	Bengine::SpriteBatch _spriteBatchHud;

	Bengine::SpriteFont* _spriteFont;

	int _nivelActual;		///< Nivel actual

	int _puntuacion;	

	Bengine::AudioEngine _audioEngine;
	Bengine::SoundEffect _soundCocoComido;
	Bengine::SoundEffect _soundFrutaComida;
	Bengine::SoundEffect _soundFantasmaComido;
	Bengine::SoundEffect _soundPacmanMuerto;
	Bengine::SoundEffect _soundVidaExtra;
	Bengine::Music _soundFantasmaVulnerable;


	EstadoJuego _estadoJuego;	

	//Condicio para ganar el juego

	//Metodos auxiliares
	std::string formatearTiempo(int tiempo);
};

