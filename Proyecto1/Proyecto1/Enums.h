#pragma once

enum class TIPO_COCO{
	NORMAL,
	SUPER,
};

enum class TIPO_FRUTA{
	CEREZA,
	FRESA,
};


enum class TIPO_FANTASMA{
	ROJO,
	CELESTE,
	AMARILLO,
	ROSA,
};

enum class ESTADO_FANTASMA{
	EN_PRISION,		//Dentro de prisi�n
	ALEATORIO,		//Movimiento aleatorio
	PERSECUCION,	//Persecucion de pacman
};

enum class ENUM_DIRECCION{
	ARRIBA,
	ABAJO,
	IZQUIERDA,
	DERECHA,
};