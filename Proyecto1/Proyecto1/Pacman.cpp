#include "Pacman.h"

#include <SDL\SDL.h>
#include <Bengine\ResourceManager.h>
#include <iostream>
#include <glm\gtx\rotate_vector.hpp>

Pacman::Pacman(int vidas)
{
	_vidas = vidas;
	_frames = 0;
}


Pacman::~Pacman()
{
}

/**
*	Inicializa los atributos de pacman
*	Creada en 25/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.3.0
*/
void Pacman::iniciar(float velocidad, glm::vec2 posicion, Bengine::InputManager* inputManager){
	_velocidad = velocidad;
	_posicion = posicion;
	_inputManager = inputManager;
	setDireccion(_direccion, ENUM_DIRECCION::IZQUIERDA);

	_poderActivo = false; 
	_tiempoRestantePoder = 0;


	_oldPosicion = _posicion;
	_oldDireccion = _direccion;
	_nextDireccion = _direccion;

	_textureID = Bengine::ResourceManager::getTexture("Texturas/pacman1.png").id;


	_color.setColor(255, 255, 255, 255);

	//static int estadoTextura = 0;

	//if (estadoTextura == 0){
	//	_textureID = Bengine::ResourceManager::getTexture("Texturas/pacman1.png").id;
	//	estadoTextura = 1;
	//}
	//else{
	//	_textureID = Bengine::ResourceManager::getTexture("Texturas/pacman2.png").id;
	//}


	
}


/**
*	Verifica si se ha presionado un bot�n de movimiento y actualiza la posici�n de pacman de forma adecuada
*	dependiendo de la velocidad.
*	Creada en 27/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.3.0
*/
void Pacman::actualizar(Nivel* nivel){

	_oldDireccion = _direccion; 
	_oldPosicion = _posicion;

	if (_inputManager->isKeyPressed(SDLK_w)){
		setDireccion(_nextDireccion, ENUM_DIRECCION::ARRIBA);
	}
	else if (_inputManager->isKeyPressed(SDLK_s)){
		setDireccion(_nextDireccion, ENUM_DIRECCION::ABAJO);
	}
	if (_inputManager->isKeyPressed(SDLK_a)){
		setDireccion(_nextDireccion, ENUM_DIRECCION::IZQUIERDA);
	}
	else if (_inputManager->isKeyPressed(SDLK_d)){
		setDireccion(_nextDireccion, ENUM_DIRECCION::DERECHA);
	}
	else{

	}

	
	_direccion = _nextDireccion;

	_posicion += _direccion * _velocidad; 

	colisionarConNivel(nivel->getNivelData());
	
	std::vector<Puerta*> puertas = nivel->getPuertas();

	for (int i = 0; i < puertas.size(); i++){
		colisionarConPuerta(puertas[i]);
	}

	//std::cout << "PACMAN - posicion:" << " x: " << _posicion.x << " ; y " << _posicion.y << std::endl;
	//std::cout << "PACMAN - oldPosicion:" << " x: " << _oldPosicion.x << " ; y " << _oldPosicion.y << std::endl;

	//if (floor(_posicion.x) == floor(_oldPosicion.x) && floor(_posicion.y) == floor(_oldPosicion.y))

	//	Usamos la velocidad de pacman como un delta para determinar si dos coordenas de posici�n son muy cercanas
	//	esto debido hay que existe ligeros errores de aproximacion de las posiciones y causaba que 
	//	no se detecte la colision adecuadamente, usualmente por diferencias muy peque�as en los flotantes
	//	Si usamos un delta mayor o igual a velocidad entonces en cada movimiento de pacman nos detectar�a
	//	verdadero al compara oldPosicion con Posicion
	if (valoresMuyCercanos(_posicion.x,_oldPosicion.x, _velocidad) && valoresMuyCercanos(_posicion.y , _oldPosicion.y, _velocidad)){		//Si colision� con algun muro
		//std::cout << "PACMAN -  posicion == _oldposicion" << std::endl;

		_nextDireccion = _direccion; 
		_direccion = _oldDireccion; 

		_posicion += _direccion * _velocidad; 
		
	}


	colisionarConNivel(nivel->getNivelData());
	

	for (int i = 0; i < puertas.size(); i++){
		colisionarConPuerta(puertas[i]);
	}

	//DEBUG
	_frames++;

	if (_frames == 10){
		_frames = 0;
	}

	//ATAJOS
	if (_posicion.y == nivel->getPosAtajo1().y){
		if (_posicion.x == nivel->getPosAtajo1().x){
			_posicion.x = nivel->getPosAtajo2().x - 3;
		}
		else if (_posicion.x == nivel->getPosAtajo2().x){
			_posicion.x = nivel->getPosAtajo1().x + 3;
		}
	}

}

void Pacman::dibujar(Bengine::SpriteBatch& spriteBatch){

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);


	glm::vec4 destRect(_posicion.x, _posicion.y, ANCHO_AGENTE, ANCHO_AGENTE);

	spriteBatch.draw(destRect, uvRect, _textureID, 0.0f, _color, _direccion);
}

//Obtener el puntaje del coco
//Producto de la colici�n entre el pacman y el coco


//Sistema de vidas del pacman
// Si lo toca un fantasma pierde 1
//Si alcanza los 10000 gana 1

//metodo para dibujarlo en ventana

/**
*	Activa los poderes de pacman
*	Creada en 06/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
bool Pacman::colisionarConElemento(ElementoEstatico* elem){

	const float DISTANCIA_MINIMA = RADIO_ELEM_ESTATICO + (RADIO_ELEM_ESTATICO/4);

	glm::vec2 centerPosA = _posicion + glm::vec2(RADIO_ELEM_ESTATICO);
	glm::vec2 centerPosB = elem->getPosicion() + glm::vec2(RADIO_ELEM_ESTATICO);

	glm::vec2 distVec = centerPosA - centerPosB;

	float distancia = glm::length(distVec);

	float collisionDepth = DISTANCIA_MINIMA - distancia;

	if (collisionDepth > 0)
	{
		return true;
	}
	return false;


}

//ARREGLAR ESTO
//bool Pacman::colisionarConFruta(Fruta* fruta){
//
//	const float DISTANCIA_MINIMA = RADIO_AGENTE + (RADIO_COCO / 4);
//
//	glm::vec2 centerPosA = _posicion + glm::vec2(RADIO_AGENTE);
//	glm::vec2 centerPosB = fruta->getPosicion() + glm::vec2(RADIO_FRUTA);
//
//	glm::vec2 distVec = centerPosA - centerPosB;
//
//	float distancia = glm::length(distVec);
//
//	float collisionDepth = DISTANCIA_MINIMA - distancia;
//
//	if (collisionDepth > 0)
//	{
//		return true;
//	}
//	return false;
//
//}

void Pacman::activarPoder(int duracion /*en fotogramas*/){
	_poderActivo = true;
	_tiempoRestantePoder = duracion;
}

/**
*	Modifica el tiempo restante de los poderes de pacman, permite tanto incrementarlo como dismuirlo
*	el caso m�s co�m ser�a disminuir en 1 fotograma, pero si como otro SuperCoco deber�a aumentarse
*	Creada en 06/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
void Pacman::modificarTiempoPoder(int delta){
	_tiempoRestantePoder += delta;

	if (_tiempoRestantePoder <= 0){
		_poderActivo = false;
	}
}
////////////////
void Pacman::animacion(){
	if (_textureID == Bengine::ResourceManager::getTexture("Texturas/pacman1.png").id){
		_textureID = Bengine::ResourceManager::getTexture("Texturas/pacman2.png").id;
	}
	else{
		_textureID = Bengine::ResourceManager::getTexture("Texturas/pacman1.png").id;
	}
}


