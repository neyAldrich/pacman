#include "Agente.h"
#include <Bengine/ResourceManager.h>
#include "Nivel.h"

#include <algorithm>
#include <ctime>
#include <random>


Agente::Agente()
{
}


Agente::~Agente()
{
}

//bool Agente::comparadorBaldosasPosicion(const glm::vec2 &baldosa1, const glm::vec2 &baldosa2){
//	float dist1 = glm::distance(baldosa1, _posicion);
//	float dist2 = glm::distance(baldosa2, _posicion);
//
//	return (dist1 < dist2);
//}

/**
*	Verifica y maneja las colisiones con las 4 baldosas m�s cercanas.
*	Creada en 30/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.3.1
*/
bool Agente::colisionarConNivel(const std::vector<std::string>& nivelData){
	std::list<glm::vec2> posBaldosasColision;

	//Primera Esquina
	verificarPosBaldosa(nivelData, 
						posBaldosasColision, 
						_posicion.x,
						_posicion.y);

	//Segunda Esquina
	verificarPosBaldosa(nivelData,
						posBaldosasColision,
						_posicion.x + ANCHO_AGENTE,
						_posicion.y);

	//Tercera Esquina
	verificarPosBaldosa(nivelData,
						posBaldosasColision,
						_posicion.x,
						_posicion.y + ANCHO_AGENTE);

	//Cuarta Esquina
	verificarPosBaldosa(nivelData,
						posBaldosasColision,
						_posicion.x + ANCHO_AGENTE,
						_posicion.y + ANCHO_AGENTE);


	if (posBaldosasColision.size() == 0){
		return false;
	}

	//Ejecutar las colisiones
	for each (glm::vec2 baldosaColision in posBaldosasColision){
		colisionarConBaldosa(baldosaColision);
	}

	return true;
}

void Agente::verificarPosBaldosa(const std::vector<std::string>& nivelData,
	std::list<glm::vec2>& posBaldosasColision,
	float x,
	float y){

	float minDist = 1;

	glm::vec2 posEsquina = glm::vec2(floor(x / ANCHO_BALDOSAS),
		floor(y / ANCHO_BALDOSAS));

	if (nivelData[posEsquina.y][posEsquina.x] == 'M'){
		glm::vec2 posBaldosa = posEsquina * (ANCHO_BALDOSAS)+glm::vec2(ANCHO_BALDOSAS / 2.0f);

		float dist = length(posBaldosa - _posicion);
		if (minDist = 0){
			minDist = dist;
		}

		if (dist >= minDist){
			posBaldosasColision.push_back(posBaldosa);
		}
		else{
			posBaldosasColision.push_front(posBaldosa);
		}

	}
}

//Axis Aligned Bounding Box Collision
void Agente::colisionarConBaldosa(glm::vec2 posBaldosa){
	
	//Eliminar const float RADIO_AGENTE = ANCHO_AGENTE/ 2 ;
	const float RADIO_BALDOSA = ANCHO_BALDOSAS / 2;
	const float DISTANCIA_MINIMA = RADIO_AGENTE + RADIO_BALDOSA;

	glm::vec2 posCentroAgente = _posicion + glm::vec2(RADIO_AGENTE);
	glm::vec2 distVec = posCentroAgente - posBaldosa;
		
	float xDepth = DISTANCIA_MINIMA - abs(distVec.x);
	float yDepth = DISTANCIA_MINIMA - abs(distVec.y);

	if (xDepth > 0 || yDepth > 0){
		if (std::max(xDepth, 0.0f) < std::max(yDepth, 0.0f)){
			if (distVec.x < 0){
				_posicion.x -= xDepth;
			}
			else{
				_posicion.x += xDepth;
			}
		}
		else{
			if (distVec.y < 0){
				_posicion.y -= yDepth;
			}
			else{
				_posicion.y += yDepth;
			}
		}
	}
}

/**
 *	Agrega la informaci�n requerida (posici�n, textura, dimensiones, color) de cada agente 
 *	al spritebatch(Lote de sprites) para que quede listo para ser renderizado.
 *	Creada en 29/05/2015
 *	Autor: Neycker Aguayo
 *	Versi�n de la aplicaci�n: 0.3.1 
*/


bool Agente::colisionarConAgente(Agente* agente){
	
	const float DISTANCIA_MINIMA = RADIO_AGENTE * 1.5f;
	
	glm::vec2 centerPosA = _posicion + glm::vec2(RADIO_AGENTE);
	glm::vec2 centerPosB = agente->getPosicion() + glm::vec2(RADIO_AGENTE);

	glm::vec2 distVec = centerPosA - centerPosB;

	float distancia = glm::length(distVec);
	
	float collisionDepth = DISTANCIA_MINIMA - distancia;

	if (collisionDepth > 0)
	{

	glm::vec2 collisionDepthVec = glm::normalize(distVec) * collisionDepth;
		
	_posicion += collisionDepthVec / 2.0f;
	agente->_posicion -= collisionDepthVec / 2.0f;

		return true;
	}
	return false;

}

void Agente::colisionarConPuerta(Puerta* puerta){

		const float DISTANCIA_MINIMA = RADIO_AGENTE + RADIO_ELEM_ESTATICO;

		glm::vec2 centerPosA = _posicion + glm::vec2(RADIO_AGENTE);
		glm::vec2 centerPosB = puerta->getPosicion() + glm::vec2(RADIO_AGENTE);

		glm::vec2 distVec = centerPosA - centerPosB;

		float distancia = glm::length(distVec);

		float collisionDepth = DISTANCIA_MINIMA - distancia;

		if (collisionDepth > 0)
		{

			glm::vec2 collisionDepthVec = glm::normalize(distVec) * collisionDepth;
			_posicion += collisionDepthVec;
		}
}

void Agente::setPosicion(glm::vec2 posicion){
	_posicion = posicion;
}

void Agente::setDireccion(glm::vec2 &direccion, ENUM_DIRECCION enum_direccion){
	switch (enum_direccion){
	case ENUM_DIRECCION::ARRIBA:
		direccion = glm::vec2(0.0f, 1.0f);
		break;
	case ENUM_DIRECCION::ABAJO:
		direccion = glm::vec2(0.0f, -1.0f);
		break;
	case ENUM_DIRECCION::IZQUIERDA:
		direccion = glm::vec2(-1.0f, 0.0f);
		break;
	case ENUM_DIRECCION::DERECHA:
		direccion = glm::vec2(1.0f, 0.0f);
		break;
	}
}

ENUM_DIRECCION Agente::generarDireccion(){
	static std::mt19937 randomEngine(time(nullptr));
	static std::uniform_int_distribution<int> randDireccion(0, 3);
	ENUM_DIRECCION nuevaDireccion = static_cast<ENUM_DIRECCION>(randDireccion(randomEngine));

	return nuevaDireccion;
}

void Agente::setTextureID(int textureID){
	_textureID = textureID;
}

/**
*	M�todo auxiliar para determinar la diferencia entre dos valores es menor a cierto delta
*	Creada en 09/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
bool Agente::valoresMuyCercanos(int x, int y, float delta){
	return (abs(x - y) < delta);
}