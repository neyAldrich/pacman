/* 	5 lineas de cabecera con informacion
	31 x 28  Baldosas - Tamaño de las baldosas: 20x20
	Significado de los simbolos:	M = Muro; . = coco; S = supercoco; @ = Pacman ; P = Muro de prision;
	D = Puerta de prision; - = espacios en blanco; F R A C = Fantasmas; 1,2 = Atajos en el laberinto; <> = Muros invisibles
*/
VVVVVVVVVVVVVVVVVVVVVVVVVVVV
V............VV............V
V.ZZZZ.ZZZZZ.VV.ZZZZZ.ZZZZ.V
VSZZZZ.ZZZZZ.VV.ZZZZZ.ZZZZSV
V.ZZZZ.ZZZZZ.VV.ZZZZZ.ZZZZ.V
V..........................V
V.ZZZZ.XX.VVVVVVVV.XX.ZZZZ.V
V.ZZZZ.XX.VVVVVVVV.XX.ZZZZ.V
V......XX....VV....XX......V
VVVVVV.XXXXX-VV-XXXXX.VVVVVV
-----V.XXXXX-VV-XXXXX.V-----
-----V.XX----------XX.V-----
-----V.XX-XXXDDXXX-XX.V-----
VVVVVV.XX-X*F**A*X-XX.VVVVVV
<1----.---X******X---.----2<
VVVVVV.XX-X*R**C*X-XX.VVVVVV
-----V.XX-XXXXXXXX-XX.V-----
-----V.XX----f-----XX.V-----
-----V.XX-VVVVVVVV-XX.V-----
VVVVVV.XX-VVVVVVVV-XX.VVVVVV
V............VV............V
V.ZZZZ.ZZZZZ.VV.ZZZZZ.ZZZZ.V
V.ZZZZ.ZZZZZ.VV.ZZZZZ.ZZZZ.V
VS..ZZ.......@........ZZ..SV
VVV.ZZ.ZZ.VVVVVVVV.ZZ.ZZ.VVV
VVV.ZZ.ZZ.VVVVVVVV.ZZ.ZZ.VVV
V......ZZ....VV....ZZ......V
V.ZZZZZZZZZZ.VV.ZZZZZZZZZZ.V
V.ZZZZZZZZZZ.VV.ZZZZZZZZZZ.V
V..........................V
VVVVVVVVVVVVVVVVVVVVVVVVVVVV