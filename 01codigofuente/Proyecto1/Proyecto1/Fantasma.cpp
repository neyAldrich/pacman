#include "Fantasma.h"
#include "Pacman.h"
#include <ctime>
#include <random>

#include <SDL\SDL.h>
#include <iostream>
#include <Bengine\ResourceManager.h>
#include <glm/gtx/rotate_vector.hpp>

Fantasma::Fantasma() : _frames(0), _tipoFantasma(TIPO_FANTASMA::ROJO)
{
	setDireccion(_direccion, ENUM_DIRECCION::ARRIBA); // Por defecto
}

Fantasma::~Fantasma()
{
}

void Fantasma::iniciar(glm::vec2 posicion, TIPO_FANTASMA tipoFantasma, float dificultad){
	
	_posicion = posicion;
	_oldPosicion = _posicion;
	_tipoFantasma = tipoFantasma;
	_deltaCercania = 8 * ANCHO_BALDOSAS;

	boolColisionarPuerta = true;
	boolMovAleatorio = true;
	boolEnPrision = true;

	std::string texturePath = "";

	switch (_tipoFantasma){
	case TIPO_FANTASMA::ROJO:
		texturePath = "Texturas/fantasmaRojo.png";
		setDireccion(_direccion, ENUM_DIRECCION::ARRIBA);
		_velocidad = 3.5f * dificultad;
		break;
	case TIPO_FANTASMA::CELESTE:
		texturePath = "Texturas/fantasmaCeleste.png";
		setDireccion(_direccion, ENUM_DIRECCION::DERECHA);
		_velocidad = 3.0f * dificultad;
		break;
	case TIPO_FANTASMA::AMARILLO:
		texturePath = "Texturas/fantasmaAmarillo.png";
		setDireccion(_direccion, ENUM_DIRECCION::ABAJO);
		_velocidad = 2.5f * dificultad;
		break;
	case TIPO_FANTASMA::ROSA:
		texturePath = "Texturas/fantasmaRosa.png";
		setDireccion(_direccion, ENUM_DIRECCION::IZQUIERDA);
		_velocidad = 2.5f * dificultad;
		break;
	}

	_oldDireccion = _direccion;
	_nextDireccion = _direccion;

	_textureID = Bengine::ResourceManager::getTexture(texturePath).id;

	_color.setColor(255, 255, 255, 255);

}

/**
*	Genera direcciones de movimiento aleatorias cada segundo y mueve al fantasma
*	Tambi�n comprueba las colisiones con el escenario
*	Creada en 27/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.3.0
*/
void Fantasma::actualizar(Nivel* nivel, Pacman* pacman){
	//Por hacer: Movimiento continuo, Escape de prisi�n, mejor b�squeda de pacman.


	//_oldPosicion = _posicion;
	//_oldDireccion = _direccion;

	//Movimiento aleatorio cada 30 frames

	//if (boolMovAleatorio){
		if (_frames == 60){
			_frames = 0;
			setDireccion(_direccion, generarDireccion());

			
			//DEBUG
			//std::cout << "Fantasma: aleatorio = TRUE " << static_cast<int>(_tipoFantasma) << std::endl;
			//std::cout << "Fantasma - direccion:" << " x: " << _direccion.x << " ; y " << _direccion.y << std::endl;
		}

		_posicion += _direccion * _velocidad;


		///Colisi�n con puertas - Por el momento desactivado

		//std::vector<Puerta*> puertas = nivel->getPuertas();
		//for (int i = 0; i < puertas.size(); i++){
		//	colisionarConPuerta(puertas[i]);
		//}

		colisionarConNivel(nivel->getNivelData());

		_frames++;

		//if (valoresMuyCercanos(_posicion.x, _oldPosicion.x, _velocidad) && valoresMuyCercanos(_posicion.y, _oldPosicion.y, _velocidad)){		//Si colision� con algun muro
		//	//std::cout << "PACMAN -  posicion == _oldposicion" << std::endl;

		//	_nextDireccion = _direccion;
		//	_direccion = _oldDireccion;

		//	_posicion += _direccion * _velocidad;

		//	while (valoresMuyCercanos(_posicion.x, _oldPosicion.x, _velocidad) && valoresMuyCercanos(_posicion.y, _oldPosicion.y, _velocidad)){		//Si colision� con algun muro

		//		_nextDireccion = _direccion;
		//		setDireccion(_direccion, generarDireccion());

		//		_posicion += _direccion * _velocidad;
		//	}
		//}
	//}

	//colisionarConNivel(nivel->getNivelData());
	

	//if (boolColisionarPuerta){

	//}
	//else if (boolEnPrision){
	//	boolMovAleatorio = false;
	//	int direccionAUsar = 0;

	//	std::vector<glm::vec2> direccionesHaciaPuerta;
	//	glm::vec2 posPuerta = nivel->getPuertas()[0]->getPosicion();

	//	direccionesHaciaPuerta = obtenerDireccionesHaciaElemento(posPuerta + glm::vec2(0.0f, ANCHO_BALDOSAS));

	//	//_direccion = direccionesHaciaPuerta[0];
	//	
	//	while (valoresMuyCercanos(_oldPosicion.x, _posicion.x, _velocidad) && 
	//		  valoresMuyCercanos(_oldPosicion.y, _posicion.y, _velocidad)){

	//		_direccion = direccionesHaciaPuerta[direccionAUsar];
	//		
	//		_posicion += _direccion * _velocidad;

	//		direccionAUsar = abs(direccionAUsar - 1);

	//		colisionarConNivel(nivel->getNivelData());
	//	}
	//	

	//	//colisionarConNivel(nivel->getNivelData());

	//	//std::cout << "Puerta - posicion:" << " x: " << posPuerta.x << " ; y " << posPuerta.y << std::endl;
	//	//std::cout << "Fantasma - Posicion:" << " x: " << _posicion.x << " ; y " << _posicion.y << std::endl;

	//	if (valoresMuyCercanos(posPuerta.x, _posicion.x, 2) && valoresMuyCercanos(posPuerta.y + ANCHO_BALDOSAS, _posicion.y, 2)){
	//		boolColisionarPuerta = true;
	//		boolMovAleatorio = true;
	//		boolEnPrision = false;
	//		//std::cout << "ColisionarConPuerta = TRUE " << static_cast<int>(_tipoFantasma) << std::endl;

	//	}
	//}

	


	//ATAJOS
	if (_posicion.y == nivel->getPosAtajo1().y && _posicion.x == nivel->getPosAtajo1().x){
		_posicion = glm::vec2(nivel->getPosAtajo2().x, nivel->getPosAtajo2().y);
		_posicion += _direccion * _velocidad;
	}
	else if (_posicion.y == nivel->getPosAtajo2().y && _posicion.x == nivel->getPosAtajo2().x){
		_posicion = glm::vec2(nivel->getPosAtajo1().x, nivel->getPosAtajo1().y);
		_posicion += _direccion * _velocidad;
	}


}

void Fantasma::dibujar(Bengine::SpriteBatch& spriteBatch){

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);


	glm::vec4 destRect(_posicion.x, _posicion.y, ANCHO_AGENTE, ANCHO_AGENTE);

	spriteBatch.draw(destRect, uvRect, _textureID, 0.0f, _color);
}

/**
*	Se encarga del comportamiento del fantasma con respecto a pacman, si este est� sin poder lo persigue,
*	si pacman est� con poder el fantasma se aleja.
*	Creada en 30/05/2015
*	Autor: Oscar Moreno
*	Versi�n de la aplicaci�n: 0.5.0
*/
void Fantasma::buscarPacman(Pacman *pacman/*, std::map<std::string, glm::vec2> posicionesMuros*/){
	
	/*static std::mt19937 randomEngine(time(nullptr));
	static std::uniform_int_distribution<int> randDireccion(0, 1);*/
	if (pacman != nullptr)
	{
		if (estaPacmanCerca(pacman, _deltaCercania)){
			glm::vec2 direccion = glm::normalize(pacman->getPosicion() - _posicion);

			if (pacman->getPoderActivo()){

				//_posicion -= direccion * _velocidad;
				//_direccion = glm::rotate(_direccion, 0.1f);
				direccion.x < 0 ? _direccion.x = glm::length(direccion) : _direccion.x = -glm::length(direccion);
				
				direccion.y < 0 ? _direccion.y = glm::length(direccion) : _direccion.y = -glm::length(direccion);
			}
			else{
				//CAMBIAR
				//_posicion += direccion * (_velocidad/3); 
				//_direccion = glm::rotate(_direccion, 0.1f);

				direccion.x < 0 ? _direccion.x = -glm::length(direccion) : _direccion.x = glm::length(direccion);

				direccion.y < 0 ? _direccion.y = -glm::length(direccion) : _direccion.y = glm::length(direccion);

			}
		}
	}

}

//
//void Fantasma::huntGhost(Pacman *pacman, std::map<std::string, glm::vec2> posicionesMuros){
//
//
//	if (pacman != nullptr)
//	{
//		glm::vec2 direccion = glm::normalize(pacman->getPosicion() - _posicion);
//
//
//
//		for (std::map<std::string, glm::vec2>::iterator it = posicionesMuros.begin(); it != posicionesMuros.end(); ++it)
//		{
//
//			std::string cadena;
//			cadena = it->first;
//
//			char c;
//			c = it->first[0];
//
//			if (c == 'M' || c == 'P')
//			{
//
//			}
//			_posicion += direccion * _velocidad;
//			_direccion = glm::rotate(_direccion, 0.1f);
//		}
//	}
//		
//}




void Fantasma::setTexturaVulnerable(){
	setTextureID(Bengine::ResourceManager::getTexture("Texturas/fantasmaV1.png").id);
}

/**
*	Reinicializa las texturas del fantasma a su textura inicial, usado para las animaciones.
*	Creada en 8/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
void Fantasma::setTexturaInicial(){
	std::string texturePath;

	switch (_tipoFantasma){	
	case TIPO_FANTASMA::ROJO:
		texturePath = "Texturas/fantasmaRojo.png";
		break;
	case TIPO_FANTASMA::CELESTE:
		texturePath = "Texturas/fantasmaCeleste.png";
		break;
	case TIPO_FANTASMA::AMARILLO:
		texturePath = "Texturas/fantasmaAmarillo.png";
		break;
	case TIPO_FANTASMA::ROSA:
		texturePath = "Texturas/fantasmaRosa.png";
		break;
	}
	
	_textureID = Bengine::ResourceManager::getTexture(texturePath).id;
}

bool Fantasma::estaPacmanCerca(Pacman* pacman, float delta){
	glm::vec2 posPacman = pacman->getPosicion();
	glm::vec2 vecDif = _posicion - posPacman;

	return (abs(vecDif.x) < delta && abs(vecDif.y) < delta);
}


/**
*	Toma un elemento y devuelve las direcciones en 'x' y en 'y' m�s cercanas
*	Creada en 8/06/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.5.0
*/
std::vector<glm::vec2> Fantasma::obtenerDireccionesHaciaElemento(glm::vec2 posElemento){
	glm::vec2 direccion1, direccion2;
	std::vector<glm::vec2> direcciones;
	glm::vec2 vecDif = posElemento - _posicion;

	if (abs(vecDif.x) > abs(vecDif.y)){
		if (vecDif.x < 0){
			setDireccion(direccion1, ENUM_DIRECCION::IZQUIERDA);
		}
		else{
			setDireccion(direccion1, ENUM_DIRECCION::DERECHA);
		}

		if (vecDif.y < 0){
			setDireccion(direccion2, ENUM_DIRECCION::ABAJO);
		}
		else{
			setDireccion(direccion2, ENUM_DIRECCION::ARRIBA);
		}
	}
	else{
		if (vecDif.y < 0){
			setDireccion(direccion1, ENUM_DIRECCION::ABAJO);
		}
		else{
			setDireccion(direccion1, ENUM_DIRECCION::ARRIBA);
		}

		if (vecDif.x < 0){
			setDireccion(direccion2, ENUM_DIRECCION::IZQUIERDA);
		}
		else{
			setDireccion(direccion2, ENUM_DIRECCION::DERECHA);
		}
	}

	direcciones.push_back(direccion1);
	direcciones.push_back(direccion2);

	return direcciones;
}

