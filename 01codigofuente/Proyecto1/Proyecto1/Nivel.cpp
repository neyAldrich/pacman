#include "Nivel.h"

#include <Bengine\BengineErrors.h>
#include <Bengine\ResourceManager.h>

#include <fstream>
#include <iostream>


/**
*	Maneja la carga de niveles desde archivo.
*	Creada en 15/05/2015
*	Autor: Neycker Aguayo
*	Versi�n de la aplicaci�n: 0.1.0
*/
Nivel::Nivel(const std::string& direccionArchivo)
{
	std::ifstream archivo;
	archivo.open(direccionArchivo);

	//Validacion de errores de carga
	if (archivo.fail()){
		Bengine::fatalError("Error al cargar el nivel en " + direccionArchivo);
	}

	//Eliminamos la cabecera para leer solo lo que nos interesa.
	std::string tmp;
	for (int i = 0; i < CABECERA_NIVEL; i++){
		std::getline(archivo, tmp);
	}

	std::vector<std::string> tmpVector;

	//Ahora se lee el formato del nivel linea a linea y se almacena en un vector de strings temporal
	//porque en los vectores solo se puede agregar al final, y el escenario quedaria de cabeza.
	while (std::getline(archivo, tmp)){
		tmpVector.push_back(tmp);
	}

	//Ahora nivelData tendr� las posiciones de las baldosas en la orientacion correcta.
	for (int i = tmpVector.size(); i > 0; i--){
		_nivelData.push_back(tmpVector[i-1]);
	}


	_spriteBatch.init();
	_spriteBatch.begin();

	//Rectangulo con las coordenadas relativas de las texturas con respecto a los sprites
	glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	//Renderizar todas las baldosas
	for (int y = 0; y < _nivelData.size(); y++){
		for (int x = 0; x < _nivelData[y].size(); x++){

			//Se toma la baldosa
			char baldosa = _nivelData[y][x];

			//Se establece el rectangulo con las coordenadas de los sprites respectivos
			glm::vec4 destRect(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS);

			Bengine::ColorRGBA8 colorMuro;
			colorMuro.setColor(0, 0, 255, 255);
			Bengine::ColorRGBA8 colorPrision;
			colorPrision.setColor(204, 0, 0, 255);
			Bengine::ColorRGBA8 black;
			black.setColor(0, 0, 0, 255);
			Bengine::ColorRGBA8 white;
			white.setColor(255, 255, 255, 255);


			switch (baldosa){
			case 'M': //Muro del nivel
				_spriteBatch.draw(destRect, 
								uvRect,
								Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
								0.0f,
								colorMuro);
				posicionesMuros.insert(std::pair<std::string,glm::vec2>(baldosa + ++pos + "", glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				break;
			case 'P': //Pared de prision
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
					0.0f,
					colorPrision);
				posicionesMuros.insert(std::pair<std::string, glm::vec2>(baldosa + ++pos + "", glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_nivelData[y][x] = 'M';
				break;
			case 'D': //Puerta de prision
				//_spriteBatch.draw(destRect,
				//	uvRect,
				//	Bengine::ResourceManager::getTexture("Texturas/puertaPrision.png").id,
				//	0.0f,
				//	white);
				//_nivelData[y][x] = 'M';
				_puertas.emplace_back(new Puerta(glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_puertas.back()->dibujar(_spriteBatch);
				_nivelData[y][x] = 'M';
				break;
			case '<': //Muro invisible
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
					10.0f,
					black);
				_nivelData[y][x] = 'M';
				break;
			case '1': //Atajo 1
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
					10.0f,
					black);
				_posAtajo1.x = x * ANCHO_BALDOSAS;
				_posAtajo1.y = y * ANCHO_BALDOSAS;
				break;
			case '2': //Atajo 2
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/rectangulo.png").id,
					10.0f,
					black);
				_posAtajo2.x = x * ANCHO_BALDOSAS;
				_posAtajo2.y = y * ANCHO_BALDOSAS;
				break;
			case '.': //Coco
				_posicionesCocos.emplace_back(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS);

				_numCocos++;

				posicionesMuros.insert(std::pair<std::string, glm::vec2>(baldosa + ++pos + "", glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				break;
			
			case 'S': //SuperCoco
				_posicionesSuperCocos.emplace_back(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS);

				_numCocos++;
				break;
			
			case 'f': //Fruta
				_posFruta = glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS);

				break;
			
			case 'F': //Fantasma Rojo
				_posicionesFantasmas.insert(std::pair<TIPO_FANTASMA,
							glm::vec2>(TIPO_FANTASMA::ROSA, glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_nivelData[y][x] = '.';

				break;

			case 'R': //Fantasma Rosa
				_posicionesFantasmas.insert(std::pair<TIPO_FANTASMA, 
							glm::vec2>(TIPO_FANTASMA::ROJO, glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_nivelData[y][x] = '.';

				break;

			case 'A': //Fantasma Amarillo
				_posicionesFantasmas.insert(std::pair<TIPO_FANTASMA,
							glm::vec2>(TIPO_FANTASMA::AMARILLO, glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_nivelData[y][x] = '.';

				break;

			case 'C': //Fantasma Celeste
				_posicionesFantasmas.insert(std::pair<TIPO_FANTASMA, 
					glm::vec2>(TIPO_FANTASMA::CELESTE, glm::vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)));
				_nivelData[y][x] = '.';

				break;

			case '@': //Pacman
				_posicionPacman.x = x * ANCHO_BALDOSAS;
				_posicionPacman.y = y * ANCHO_BALDOSAS;

				_nivelData[y][x] = '.';
				break;

			case 'X': //Otra textura
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/glass.png").id,
					0.0f,
					white);
				_nivelData[y][x] = 'M';
				break;
			case 'Z': //Otra textura
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/red_bricks.png").id,
					0.0f,
					white);
				_nivelData[y][x] = 'M';
				break;
			case 'V': //Otra textura
				_spriteBatch.draw(destRect,
					uvRect,
					Bengine::ResourceManager::getTexture("Texturas/light_bricks.png").id,
					0.0f,
					white);
				_nivelData[y][x] = 'M';
				break;
			default:
				//std::printf("Unexpected symbol %c at (%d,%d)\n", baldosa, x, y);
				_nivelData[y][x] = '.';
				break;
			}
		}
	}

	_spriteBatch.end();
}


Nivel::~Nivel()
{
}

//Renderiza el lote de sprites de los muros del nivel
 void Nivel::dibujar(){
	 _spriteBatch.renderBatch();
}
