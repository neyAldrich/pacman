#pragma once

#include <vector>
#include <cstring>
#include <map>
#include "Coco.h"
#include "Enums.h"
#include "Puerta.h"

#include <Bengine\SpriteBatch.h>

const float ANCHO_BALDOSAS = 20;
const int CABECERA_NIVEL = 5;

class Nivel
{
public:

	//Cargar el nivel desde archivo
	Nivel(const std::string& direccionArchivo);

	~Nivel();

	void dibujar();

	//Getter
	glm::ivec2 getPosicionInicialPacman() const { return _posicionPacman; }
	const std::vector<std::string>& getNivelData() const { return _nivelData; }

	const std::map<TIPO_FANTASMA, glm::vec2> getPosInicialFantasmas() const { return _posicionesFantasmas; }

	std::vector<glm::ivec2> getPosicionesCocos() { return _posicionesCocos; }
	std::vector<glm::ivec2> getPosicionesSuperCocos() { return _posicionesSuperCocos; }

	int getNumCocos() { return _numCocos; }

	glm::vec2 getPosAtajo1() { return _posAtajo1; }
	glm::vec2 getPosAtajo2() { return _posAtajo2; }
	glm::vec2 getPosFruta() { return _posFruta; }

	std::map<std::string, glm::vec2> posicionesMuros;

	std::vector<Puerta*> getPuertas() { return _puertas; }

private:
	std::vector<std::string> _nivelData;

	int _numCocos;

	Bengine::SpriteBatch _spriteBatch;

	glm::ivec2 _posicionPacman;
	
	std::vector<glm::ivec2> _posicionesCocos;
	std::vector<glm::ivec2> _posicionesSuperCocos;

	std::map<TIPO_FANTASMA, glm::vec2> _posicionesFantasmas;
	
	std::vector<Puerta*> _puertas;

	glm::vec2 _posAtajo1;
	glm::vec2 _posAtajo2;

	glm::vec2 _posFruta;

	int pos; 
};

