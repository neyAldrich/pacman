#pragma once

#include <glm\glm.hpp>
#include <Bengine\SpriteBatch.h>
#include <Bengine\Vertex.h>

#include "Enums.h"
#include "ElementoEstatico.h"

const float ANCHO_FRUTA = 20;
const float RADIO_FRUTA = ANCHO_FRUTA / 2;

class Fruta : public ElementoEstatico
{
public:
	Fruta(glm::ivec2 pos, TIPO_FRUTA tipo);
	~Fruta();

	int getPuntuacion() { return _puntuacion; }

	TIPO_FRUTA getTipo() { return _tipo; }

private:

	int _puntuacion;

	TIPO_FRUTA _tipo;
};

