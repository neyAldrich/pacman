#pragma once

#include <glm\glm.hpp>
#include <Bengine\SpriteBatch.h>
#include <Bengine\Vertex.h>
#include <list>

#include "Nivel.h"
#include "Enums.h"

const float ANCHO_AGENTE = 20;
const float RADIO_AGENTE = ANCHO_AGENTE / 2;

class Fantasma;
class Pacman;

//static bool comparadorBaldosasPosicion(const glm::vec2 &baldosa1, const glm::vec2 &baldosa2,const Agente &agente){
//	float dist1 = glm::distance(baldosa1, agente.getPosicion());
//	float dist2 = glm::distance(baldosa2, agente.getPosicion());
//
//	return (dist1 < dist2);
//}


class Agente
{
public:
	Agente();
	virtual ~Agente();

	//Modificado 30/05/2015
	//MODIFICADO 31/05/2015 - Ahora se pasa un puntero de nivel, para poder obtener la informacion de las
	//	posiciones de los atajos
	void actualizar(Nivel* nivel);


	bool colisionarConNivel(const std::vector<std::string>& nivelData);

	
	//Colision entre agentes
	bool colisionarConAgente(Agente* agente);

	virtual void dibujar(Bengine::SpriteBatch& spriteBatch) = 0;

	glm::vec2 getPosicion() const { return _posicion; }
	glm::vec2 getDireccion() const { return _direccion; }

	void setPosicion(glm::vec2 posicion);


protected:

	glm::vec2 _posicion;

	Bengine::ColorRGBA8 _color;

	float _velocidad;
	glm::vec2 _direccion;

	//Cambiado: ahora cada agente tendr� su propia textura.
	int _textureID;
	void setTextureID(int textureID);

	void colisionarConBaldosa(glm::vec2 posBaldosa);
	void colisionarConPuerta(Puerta* puerta);

	void verificarPosBaldosa(const std::vector<std::string>& nivelData,
								std::list<glm::vec2>& posBaldosaColision, 
								float x, 
								float y);


	void setDireccion(glm::vec2 &direccion, ENUM_DIRECCION enum_direccion);

	ENUM_DIRECCION generarDireccion();

	bool valoresMuyCercanos(int x, int y, float delta);

	//bool comparadorBaldosasPosicion(const glm::vec2 &baldosa1,const glm::vec2 &baldosa2);
};

