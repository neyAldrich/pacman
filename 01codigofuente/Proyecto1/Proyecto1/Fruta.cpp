#include "Fruta.h"

#include <Bengine\ResourceManager.h>

Fruta::Fruta(glm::ivec2 pos, TIPO_FRUTA tipo) : ElementoEstatico(pos), _tipo(tipo)
{

	switch (_tipo){
	case TIPO_FRUTA::CEREZA:
		_textureID = Bengine::ResourceManager::getTexture("Texturas/cereza.png").id;
		_puntuacion = 100;
		break;
	case TIPO_FRUTA::FRESA:
		_textureID = Bengine::ResourceManager::getTexture("Texturas/fresa.png").id;
		_puntuacion = 500;
		break;
	}


}


Fruta::~Fruta()
{
}

