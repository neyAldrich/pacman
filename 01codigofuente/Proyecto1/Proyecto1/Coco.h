#pragma once

#include <glm\glm.hpp>
#include <Bengine\SpriteBatch.h>
#include <Bengine\Vertex.h>

#include "Enums.h"
#include "ElementoEstatico.h"

const float ANCHO_COCO = 20;
const float RADIO_COCO = ANCHO_COCO / 2;

class Coco : public ElementoEstatico
{
public:
	Coco(glm::ivec2 pos, TIPO_COCO tipo);
	~Coco();

	TIPO_COCO getTipo() { return _tipo; }

private:

	TIPO_COCO _tipo;
};

